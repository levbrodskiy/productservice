package com.team.productservice.response;

import lombok.Data;

import java.math.BigDecimal;
import java.time.Duration;

@Data
public class ProductResponse {
    private Long id;
    private String title;
    private String description;
    private BigDecimal sale;
    private BigDecimal price;
    private Long duration;
    private String imageLocation;
}
