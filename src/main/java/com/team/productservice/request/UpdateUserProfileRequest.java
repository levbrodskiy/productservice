package com.team.productservice.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class UpdateUserProfileRequest {
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String firstName;
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String secondName;
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String lastName;
    private String phone;
}
