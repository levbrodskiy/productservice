package com.team.productservice.service;

import com.team.productservice.domain.Role;
import com.team.productservice.domain.RoleName;
import com.team.productservice.domain.User;
import com.team.productservice.repository.RoleRepository;
import com.team.productservice.mapper.UserMapper;
import com.team.productservice.repository.UserRepository;
import com.team.productservice.request.UpdateUserProfileRequest;
import com.team.productservice.response.UserProfileResponse;
import com.team.productservice.service.exception.EntityAlreadyExistsException;
import com.team.productservice.service.exception.EntityNotFoundException;
import com.team.productservice.request.RegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserMapper userMapper, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.roleRepository = roleRepository;
    }


    @Transactional
    public void register(RegistrationRequest request) throws EntityAlreadyExistsException, EntityNotFoundException {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new EntityAlreadyExistsException("User with this email already exists");
        }

        Role role = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new EntityNotFoundException("Role not found"));
        System.out.println(role);
        User user = new User();
        user.addRole(role);
        userMapper.mergeIntoUser(user, request);
        System.out.println(user);

        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public UserProfileResponse getProfile(String email) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not not found"));

        return userMapper.toUserProfileResponse(user);
    }

    @Transactional
    public UserProfileResponse updateProfile(String email, UpdateUserProfileRequest request) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not not found"));

        userMapper.mergeIntoUser(user, request);

        userRepository.save(user);

        return userMapper.toUserProfileResponse(user);
    }

    @Transactional(readOnly = true)
    public boolean isAdmin(String email) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not not found"));

        return user.roleExists(RoleName.ROLE_ADMIN);
    }
}
