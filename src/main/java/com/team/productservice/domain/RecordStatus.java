package com.team.productservice.domain;

public enum RecordStatus {
    REVOKED, PROVIDED, WAITING
}
