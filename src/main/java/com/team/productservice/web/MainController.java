package com.team.productservice.web;

import com.team.productservice.service.UserService;
import com.team.productservice.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping(value = "/")
public class MainController {
    private final UserService userService;

    @Autowired
    public MainController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getMainPage(@AuthenticationPrincipal Principal principal, Model model) throws EntityNotFoundException {
        System.out.println("!");
        System.out.println(principal);
        System.out.println(userService);
        boolean isAdmin = userService.isAdmin(principal.getName());
        model.addAttribute("isAdmin", isAdmin );
        return "home";
    }
}
