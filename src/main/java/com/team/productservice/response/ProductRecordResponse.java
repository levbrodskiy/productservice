package com.team.productservice.response;

import com.team.productservice.domain.Product;
import com.team.productservice.domain.RecordStatus;
import lombok.Data;

import java.util.Date;

@Data
public class ProductRecordResponse {
    private Long id;
    private RecordStatus status;
    private Product product;
    private Date date;
    private Date createdAt;
    private Date updatedAt;
    //услуга
}
