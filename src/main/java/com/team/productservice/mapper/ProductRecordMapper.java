package com.team.productservice.mapper;

import com.team.productservice.domain.ProductRecord;
import com.team.productservice.response.ProductRecordResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductRecordMapper {

    ProductRecordResponse toResponse(ProductRecord record);
}
