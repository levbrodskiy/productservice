package com.team.productservice.request;

import lombok.Data;

@Data
public class ImageRequest {
    private String name;
    private Long productId;
    private byte[] image;
}
