package com.team.productservice.request;

import lombok.Data;

import java.util.Date;

@Data
public class AddProductRecordRequest {
    private Long userId;
    private Long productId;
    private Date date;
}
