package com.team.productservice.response;

import com.team.productservice.domain.RoleName;
import lombok.Data;

@Data
public class UserProfileResponse {
    private Long id;
    private String firstName;
    private String secondName;
    private String lastName;
    private String email;
    private String phone;
}
