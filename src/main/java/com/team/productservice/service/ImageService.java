package com.team.productservice.service;

import com.team.productservice.domain.Image;
import com.team.productservice.domain.Product;
import com.team.productservice.domain.User;
import com.team.productservice.repository.ImageRepository;
import com.team.productservice.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

@Service
public class ImageService {

    @Value("${resources.images.base-location}")
    private String baseLocation;

    private final ImageRepository imageRepository;

    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Transactional
    public Image addImage(byte[] data) throws IOException {
        Image image = new Image();
        image.setId(null);

        image = imageRepository.save(image);

        String location = getLocation(image.getId());

        image.setLocation(location);

        Files.write(Paths.get(location), data, StandardOpenOption.CREATE_NEW);
        image = imageRepository.save(image);
        System.out.println(Paths.get(image.getLocation()).toAbsolutePath());
        return image;
    }

    @Transactional
    public void updateImage(Long productId, Long imageId, String name, byte[] data)
            throws EntityNotFoundException, IOException {

        Image image = imageRepository
                .findById(imageId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Files.deleteIfExists(Paths.get(image.getLocation()));

        String location = getLocation(imageId);

        Files.write(Paths.get(location), data, StandardOpenOption.CREATE_NEW);

        image.setName(name);
        image.setLocation(location);

        imageRepository.save(image);
    }

    @Transactional
    public void deleteImage(Long id) throws EntityNotFoundException, IOException {
        Image image = imageRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(""));

        Files.deleteIfExists(Paths.get(image.getLocation()));
    }

    @Transactional
    public String getImagePath(Long imageId) throws EntityNotFoundException {
        Image image = imageRepository.findById(imageId)
                .orElseThrow(() -> new EntityNotFoundException(""));

        return image.getLocation();
    }

    private String getLocation(Long imageId) {
        String location = Paths
                .get(baseLocation, imageId.toString() +  ".png")
                .toAbsolutePath()
                .toString();

        return location;
    }
}
