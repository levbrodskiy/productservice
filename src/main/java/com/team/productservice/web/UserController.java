package com.team.productservice.web;

import com.team.productservice.domain.User;
import com.team.productservice.request.UpdateUserProfileRequest;
import com.team.productservice.response.UserProfileResponse;
import com.team.productservice.service.UserService;
import com.team.productservice.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/profile")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getProfile(Model model, Model error,
                             @AuthenticationPrincipal Principal principal) {
        try {
            UserProfileResponse profile = userService.getProfile(principal.getName());
            boolean isAdmin = userService.isAdmin(principal.getName());
            System.out.println(profile);
            System.out.println(isAdmin);
            model.addAttribute("user", profile);
            model.addAttribute("isAdmin", isAdmin);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка вывода ");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "profile";
    }

    @GetMapping("/edit")
    public String editProfilePage(Model model,Model error,
                                  @AuthenticationPrincipal Principal principal) {
        try {
            UserProfileResponse profile = userService.getProfile(principal.getName());
            boolean isAdmin = userService.isAdmin(principal.getName());
            model.addAttribute("user", profile);
            model.addAttribute("isAdmin", isAdmin);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка редактирования");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "profile_edit";
    }

    @PostMapping("/edit")
    public String editProfile(Model model,Model error, @AuthenticationPrincipal Principal principal,
                              UpdateUserProfileRequest request) {
        try {
            userService.updateProfile(principal.getName(), request);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка редактирования");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "redirect:/profile";
    }
}
