package com.team.productservice.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class RegistrationRequest {
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String firstName;
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String secondName;
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String lastName;
    @NotBlank
    @Email
    @Min(value = 8)
    @Max(value = 256)
    private String email;
    @NotBlank
    @Min(value = 11)
    @Max(value = 14)
    private String phone;
    @NotBlank
    @Min(value = 8)
    @Max(value = 256)
    private String password;
}
