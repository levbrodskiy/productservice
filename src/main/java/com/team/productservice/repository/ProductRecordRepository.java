package com.team.productservice.repository;

import com.team.productservice.domain.Product;
import com.team.productservice.domain.ProductRecord;
import com.team.productservice.domain.RecordStatus;
import com.team.productservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRecordRepository extends JpaRepository<ProductRecord, Long> {
    boolean existsByUserAndProductAndDate(User user, Product product, Date date);
    List<ProductRecord> findByUser(User user);
    Optional<ProductRecord> findByIdAndUser(Long id, User user);
    boolean existsByIdAndUser(Long id, User user);
    boolean existsByProductAndStatus(Product product, RecordStatus status);
}
