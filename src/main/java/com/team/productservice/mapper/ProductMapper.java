package com.team.productservice.mapper;

import com.team.productservice.domain.Product;
import com.team.productservice.request.ProductRequest;
import com.team.productservice.response.ProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.Duration;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    void mergeIntoProduct(@MappingTarget Product product, ProductRequest request);
    @Mapping(target = "duration", ignore = true)
    @Mapping(target = "imageLocation", source = "image.location")
    ProductResponse toResponse(Product product);
    default Duration map(Long min) {
        return Duration.ofMinutes(min);
    }
}
