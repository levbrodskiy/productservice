package com.team.productservice.web;

import com.team.productservice.request.RegistrationRequest;
import com.team.productservice.service.UserService;
import com.team.productservice.service.exception.EntityAlreadyExistsException;
import com.team.productservice.service.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController {
    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getRegistrationPage() {
        return "registration";
    }

    @PostMapping
    public String registration(Model model, RegistrationRequest request) {

        try {
            userService.register(request);
        } catch (EntityAlreadyExistsException e) {
            model.addAttribute("title","Ошибка регистрации");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка регистрации");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }
// тут должна быть обработка ошибки
        return "redirect:/profile";
    }
}
