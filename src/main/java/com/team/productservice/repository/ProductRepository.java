package com.team.productservice.repository;

import com.team.productservice.domain.Product;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    boolean existsById(Long id);
    List<Product> findByTitleContainsAndSaleBetween(String title, BigDecimal start, BigDecimal end, Sort sort);
    List<Product> findBySaleBetween(BigDecimal start, BigDecimal end, Sort sort);
    List<Product> findByTitleContains(String title, Sort sort);
}
