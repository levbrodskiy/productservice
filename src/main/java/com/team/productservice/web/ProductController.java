package com.team.productservice.web;

import com.team.productservice.request.ProductRequest;
import com.team.productservice.response.ProductResponse;
import com.team.productservice.service.ProductService;
import com.team.productservice.service.UserService;
import com.team.productservice.service.exception.EntityNotFoundException;
import com.team.productservice.service.exception.PermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/products")
public class ProductController {
    private final ProductService productService;
    private final UserService userService;

    @Autowired
    public ProductController(ProductService productService, UserService userService) {
        this.productService = productService;
        this.userService = userService;
    }

    @GetMapping
    public String getProductsPage(Model model,
                                  @AuthenticationPrincipal Principal principal,
                                  @RequestParam(name = "title", required = false, defaultValue = "") String title,
                                  @RequestParam(name = "filter_sale", required = false, defaultValue = "0-100") String filter,
                                  @RequestParam(name = "price_sort", required = false) String sort) {

        List<ProductResponse> products = new ArrayList<>();
        Sort priceSort = null;

        System.out.println(title);
        if (title != null && title.equals("")) {
            title = null;
        }

        if (sort != null && sort.equals("asc")) {
            priceSort = Sort.by("price").ascending();
        } else if (sort != null && sort.equals("desc")) {
            priceSort = Sort.by("price").descending();
        } else {
            priceSort = Sort.unsorted();
        }
        System.out.println(title);
        System.out.println(filter);
        System.out.println(sort);

        BigDecimal minSale = null;
        BigDecimal maxSale = null;

        try {
            String[] filters = filter.split("to");
            minSale = BigDecimal.valueOf(Double.valueOf(filters[0]));
            maxSale = BigDecimal.valueOf(Double.valueOf(filters[1]));
        } catch (Exception e) {
            System.out.println("here");

        }
        System.out.println(priceSort);
        if (title != null && minSale == null && maxSale == null) {
            products = productService.getProductsLikeTitle(title, priceSort);
        } else if (title == null && minSale != null && maxSale != null) {
            products = productService.getProductsBetweenSale(minSale, maxSale, priceSort);
        } else if (title != null && minSale != null && maxSale != null) {
            products = productService.getProductsLikeTitleAndSaleBetween(title, minSale, maxSale, priceSort);
        } else {
            products = productService.getProductsBetweenSale(new BigDecimal(0), new BigDecimal(100), priceSort);
        }
        System.out.println(products);

        boolean isAdmin = false;
        try {
            long productsCount = productService.getProductCount();
            isAdmin = userService.isAdmin(principal.getName());
            model.addAttribute("products", products);
            model.addAttribute("isAdmin", isAdmin);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка при загрузке страницы");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }


        return "services";
    }

    @GetMapping("/{id}")
    public String getProductPage(Model model,
                                 @AuthenticationPrincipal Principal principal,
            @PathVariable(name = "id") Long id) {

        boolean isAdmin = false;
        try {
            ProductResponse product = productService.getProduct(id);
            isAdmin = userService.isAdmin(principal.getName());
            System.out.println("!");
            System.out.println(product);
            model.addAttribute("product", product);
            model.addAttribute("isAdmin", isAdmin);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка вывода услуг");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "service";
    }

    @GetMapping("/add")
    public String getAddProductPage() {
        return "service_add";
    }

    @PostMapping("/add_product")
    public String addProduct(Model model,@AuthenticationPrincipal Principal principal,
                             ProductRequest request,
                             @RequestParam(name = "images")MultipartFile image) {
        try {
            System.out.println(image.getName());
            System.out.println(image.getSize());
            System.out.println(image.isEmpty());
            productService.addProduct(principal.getName(), request, image);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка добавления");
            model.addAttribute("description","Попробуйте повторить позже");
            return "error";
        } catch (PermissionException e) {
            model.addAttribute("title","Ошибка добавления");
            model.addAttribute("description","Попробуйте повторить позже");
            return "error";
        }

        return "redirect:/products";
    }

    @GetMapping("/{id}/edit")
    public String getEditProductPage(Model model,
                                     @AuthenticationPrincipal Principal principal,

                                     @PathVariable(name = "id") Long id) {

        boolean isAdmin = false;
        try {
            ProductResponse product = productService.getProduct(id);
            isAdmin = userService.isAdmin(principal.getName());
            System.out.println("!");
            System.out.println(product);
            model.addAttribute("product", product);
            model.addAttribute("isAdmin", isAdmin);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return "service_edit";
    }

    @PostMapping("/{id}/update")
    public String editProduct(@AuthenticationPrincipal Principal principal,
                              @PathVariable(name = "id") Long id,
                              ProductRequest request) {
        try {
            System.out.println("here");
            productService.updateProduct(principal.getName(), id, request);

        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (PermissionException e) {
            e.printStackTrace();
        }
        return "redirect:/products";
    }
    @DeleteMapping("/{id}")
    public String deleteProduct(Model model, @AuthenticationPrincipal Principal principal,
                                @PathVariable(name = "id") Long id) {

        try {
            productService.deleteProduct(principal.getName(), id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка удаления");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        } catch (PermissionException e) {
            model.addAttribute("title","Ошибка удаления");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "redirect:/products";
    }
}
