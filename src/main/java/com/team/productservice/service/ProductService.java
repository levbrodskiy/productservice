package com.team.productservice.service;

import com.team.productservice.domain.*;
import com.team.productservice.mapper.ProductMapper;
import com.team.productservice.repository.ProductRecordRepository;
import com.team.productservice.repository.ProductRepository;
import com.team.productservice.repository.UserRepository;
import com.team.productservice.request.ProductRequest;
import com.team.productservice.response.ProductResponse;
import com.team.productservice.service.exception.EntityNotFoundException;
import com.team.productservice.service.exception.PermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final ProductRecordRepository productRecordRepository;
    private final ImageService imageService;

    @Autowired
    public ProductService(UserRepository userRepository, ProductRepository productRepository, ProductMapper productMapper, ProductRecordRepository productRecordRepository, ImageService imageService) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.productRecordRepository = productRecordRepository;
        this.imageService = imageService;
    }

    @Transactional
    public void addProduct(String email, ProductRequest request) throws EntityNotFoundException, PermissionException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with this email not found"));

        if (!user.roleExists(RoleName.ROLE_ADMIN)) {
            throw new PermissionException("ROLE_ADMIN is required");
        }

        Product product = new Product();
        product.setDuration(Duration.ofMinutes(request.getDuration()));
        productMapper.mergeIntoProduct(product, request);

        productRepository.save(product);
    }

    @Transactional
    public void addProduct(String email, ProductRequest request, MultipartFile image)
            throws EntityNotFoundException, PermissionException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with this email not found"));

        if (!user.roleExists(RoleName.ROLE_ADMIN)) {
            throw new PermissionException("ROLE_ADMIN is required");
        }

        Product product = new Product();
        product.setDuration(Duration.ofMinutes(request.getDuration()));
        productMapper.mergeIntoProduct(product, request);

        try {
            Image savedImage = imageService.addImage(image.getBytes());
            product.setImage(savedImage);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

        productRepository.save(product);
    }

    @Transactional
    public void updateProduct(
            String email, Long productId, ProductRequest request
    ) throws EntityNotFoundException, PermissionException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        if (!user.roleExists(RoleName.ROLE_ADMIN)) {
            throw new PermissionException("ROLE_ADMIN is required");
        }

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product with this id not found"));

        productMapper.mergeIntoProduct(product, request);

        productRepository.save(product);
    }

    @Transactional(readOnly = true)
    public List<ProductResponse> getProductsLikeTitleAndSaleBetween(
            String title, BigDecimal minSale, BigDecimal maxSale, Sort sort
    ) {
        return productRepository
                .findByTitleContainsAndSaleBetween("%" + title + "%", minSale, maxSale, sort)
                .stream()
                .map(productMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ProductResponse> getProductsBetweenSale(BigDecimal minSale, BigDecimal maxSale, Sort sort) {
        return productRepository.findBySaleBetween(minSale, maxSale, sort)
                .stream()
                .map(productMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ProductResponse> getProductsLikeTitle(String title, Sort sort) {
        return productRepository.findByTitleContains("%" + title + "%", sort)
                .stream()
                .map(productMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Long getProductCount() {
        return productRepository.count();
    }

    @Transactional
    public void deleteProduct(String email, Long id) throws EntityNotFoundException, PermissionException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        if (!user.roleExists(RoleName.ROLE_ADMIN)) {
            throw new PermissionException("ROLE_ADMIN is required");
        }

        Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product with this id not found"));

        if (!productRecordRepository.existsByProductAndStatus(product, RecordStatus.WAITING)) {
            productRepository.delete(product);
        } else {
            throw new PermissionException();
        }
    }

    @Transactional(readOnly = true)
    public ProductResponse getProduct(Long id) throws EntityNotFoundException {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product with this role not found"));

        return productMapper.toResponse(product);
    }
}
