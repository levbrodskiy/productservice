package com.team.productservice.domain;

public enum  RoleName {
    ROLE_USER, ROLE_ADMIN
}
