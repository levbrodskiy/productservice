package com.team.productservice.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.Duration;

@Data
public class ProductRequest {
    @NotBlank
    @Min(value = 3)
    @Max(value = 256)
    private String title;
    private String description;
    @NotBlank
    @Min(value = 0)
    @Max(value = 100)
    private BigDecimal sale;
    @NotBlank
    @Min(value = 0)
    private BigDecimal price;
    @NotBlank
    private Long duration;
}
