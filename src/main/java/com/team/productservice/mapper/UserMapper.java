package com.team.productservice.mapper;

import com.team.productservice.domain.User;
import com.team.productservice.request.RegistrationRequest;
import com.team.productservice.request.UpdateUserProfileRequest;
import com.team.productservice.response.UserProfileResponse;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserProfileResponse toUserProfileResponse(User user);

    void mergeIntoUser(@MappingTarget User user, RegistrationRequest request);

    void mergeIntoUser(@MappingTarget User user, UpdateUserProfileRequest request);
}
