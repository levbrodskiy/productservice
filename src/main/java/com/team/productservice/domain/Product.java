package com.team.productservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private BigDecimal sale;
    private BigDecimal price;
    private Duration duration;
    @OneToOne(fetch = FetchType.EAGER)
    private Image image;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt;
    @Temporal(value = TemporalType.DATE)
    @Column(name = "updated_at")
    private Date updatedAt;
}
