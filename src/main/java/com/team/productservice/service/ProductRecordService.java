package com.team.productservice.service;

import com.team.productservice.domain.*;
import com.team.productservice.mapper.ProductRecordMapper;
import com.team.productservice.repository.ProductRecordRepository;
import com.team.productservice.repository.ProductRepository;
import com.team.productservice.repository.UserRepository;
import com.team.productservice.response.ProductRecordResponse;
import com.team.productservice.service.exception.EntityAlreadyExistsException;
import com.team.productservice.service.exception.EntityNotFoundException;
import com.team.productservice.service.exception.PermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductRecordService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ProductRecordRepository productRecordRepository;
    private final ProductRecordMapper productRecordMapper;

    @Autowired
    public ProductRecordService(UserRepository userRepository, ProductRepository productRepository, ProductRecordRepository productRecordRepository, ProductRecordMapper productRecordMapper) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.productRecordRepository = productRecordRepository;
        this.productRecordMapper = productRecordMapper;
    }

    @Transactional
    public ProductRecordResponse addProductRecord(String email, Long productId, Date date) throws EntityNotFoundException, EntityAlreadyExistsException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product with this id not found"));

        date.setSeconds(0);

        if (productRecordRepository.existsByUserAndProductAndDate(user, product, date)) {
            throw new EntityAlreadyExistsException("Product record already exists");
        }

        ProductRecord record = new ProductRecord();
        record.setId(null);
        record.setDate(date);
        record.setProduct(product);
        record.setUser(user);
        record.setStatus(RecordStatus.WAITING);

        ProductRecord savedRecord = productRecordRepository.save(record);
        return productRecordMapper.toResponse(savedRecord);
    }

    @Transactional(readOnly = true)
    public List<ProductRecordResponse> getProductRecordsForUser(String email) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        return productRecordRepository.findByUser(user)
                .stream()
                .map(productRecordMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ProductRecordResponse> getProductRecords(String email) throws EntityNotFoundException, PermissionException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        if (!user.roleExists(RoleName.ROLE_ADMIN)) {
            throw new PermissionException();
        }

        return productRecordRepository.findAll()
                .stream()
                .map(productRecordMapper::toResponse)
                .collect(Collectors.toList());
    }

    @Transactional
    public void setProductRecordStatus(String email, Long recordId, RecordStatus status) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        if (user.roleExists(RoleName.ROLE_ADMIN) ||
        productRecordRepository.existsByIdAndUser(recordId, user)) {
            ProductRecord record = productRecordRepository.findByIdAndUser(recordId, user)
                    .orElseThrow(() -> new EntityNotFoundException(""));

            record.setStatus(status);

            productRecordRepository.save(record);
        } else {
            throw new EntityNotFoundException("");
        }
    }

    @Transactional(readOnly = true)
    public ProductRecordResponse getProductRecord(String email, Long id) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with this email not found"));

        if (user.roleExists(RoleName.ROLE_ADMIN) ||
                productRecordRepository.existsByIdAndUser(id, user)) {
            ProductRecord record = productRecordRepository.findByIdAndUser(id, user)
                    .orElseThrow(() -> new EntityNotFoundException(""));

            return productRecordMapper.toResponse(record);
        } else {
            throw new EntityNotFoundException();
        }
    }
}
