package com.team.productservice.web;

import com.team.productservice.domain.RecordStatus;
import com.team.productservice.response.ProductRecordResponse;
import com.team.productservice.service.ProductRecordService;
import com.team.productservice.service.exception.EntityAlreadyExistsException;
import com.team.productservice.service.exception.EntityNotFoundException;
import com.team.productservice.service.exception.PermissionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/productrecords")
public class ProductRecordController {
    private final ProductRecordService productRecordService;

    @Autowired
    public ProductRecordController(ProductRecordService productRecordService) {
        this.productRecordService = productRecordService;
    }

    @GetMapping
    public String getProductRecords(Model model,@AuthenticationPrincipal Principal principal) {
        try {
            List<ProductRecordResponse> records = productRecordService
                    .getProductRecordsForUser(principal.getName());
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка вывода");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "user_services";
    }

    @GetMapping("/all")
    public String getAllProductRecords(Model model, @AuthenticationPrincipal Principal principal) {
        try {
            List<ProductRecordResponse> records = productRecordService
                    .getProductRecords(principal.getName());
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка вывода");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        } catch (PermissionException e) {
            model.addAttribute("title","Ошибка вывода");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "user_services";
    }

    //TODO: set status

    @GetMapping("/{id}")
    public String getProductRecord(@AuthenticationPrincipal Principal principal,
                                   @PathVariable(name = "id") Long id) {
        try {
            ProductRecordResponse record = productRecordService
                    .getProductRecord(principal.getName(), id);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return "service";
    }

    @PostMapping("/{id}/revoke")
    public String revokeProductRecord(Model model, @AuthenticationPrincipal Principal principal,
                                      @PathVariable(name = "id") Long id) {
        try {
            productRecordService.setProductRecordStatus(principal.getName(), id, RecordStatus.REVOKED);
        } catch (EntityNotFoundException e) {
                model.addAttribute("title","Ошибка удаления");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "redirect:/productrecords";
    }

    @PostMapping("/{id}/")
    public String provideProductRecord(Model model, @AuthenticationPrincipal Principal principal,
                                      @PathVariable(name = "id") Long id) {
        try {
            productRecordService.setProductRecordStatus(principal.getName(), id, RecordStatus.PROVIDED);
        } catch (EntityNotFoundException e) {
            model.addAttribute("title","Ошибка установления статуса");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "redirect:/productrecords";
    }

    @PostMapping("/add")
    public String provideProductRecord(Model model, @AuthenticationPrincipal Principal principal,
                                       @RequestParam(name = "product_id") Long id,
                                       @RequestParam(name = "date") Date date) {
        try {
            productRecordService.addProductRecord(principal.getName(), id, date);
        } catch (EntityNotFoundException | EntityAlreadyExistsException e) {
            model.addAttribute("title","Ошибка обавления");
            model.addAttribute("description","Похоже что-то пошло не так");
            return "error";
        }

        return "redirect:/productrecords";
    }
}
